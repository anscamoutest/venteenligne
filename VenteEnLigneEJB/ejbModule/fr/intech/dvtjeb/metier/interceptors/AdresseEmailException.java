package fr.intech.dvtjeb.metier.interceptors;

public class AdresseEmailException extends Exception{
	
	AdresseEmailException(String message) {	
		super ( message );
	}
}
