package fr.intech.dvtejb.metier.sessions;

import javax.ejb.Local;

@Local
public interface CompteurStatefulLocal {

	public int incrementer();
    public void raz();

}
