package fr.intech.dvtejb.metier.sessions;

import javax.ejb.Remote;

@Remote
public interface CommandeDAO {
	
	void traitementCarteCredit();
}
