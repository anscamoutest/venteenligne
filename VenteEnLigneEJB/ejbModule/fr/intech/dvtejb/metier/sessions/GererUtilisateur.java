package fr.intech.dvtejb.metier.sessions;

import fr.intech.dvtejb.metier.entities.Utilisateur;

public interface GererUtilisateur {
	
	void ajouter(Utilisateur utilisateur);
	void supprimer(Utilisateur utilisateur);
	void modifier(Utilisateur utilisateur);
}
