package fr.intech.dvtejb.metier.sessions;

import java.math.BigDecimal;
import java.util.List;

import fr.intech.dvtejb.metier.dtos.MagasinDTO;
import fr.intech.dvtejb.metier.entities.Article;
import fr.intech.dvtejb.metier.entities.Catalogue;
import fr.intech.dvtejb.metier.entities.Produit;

public interface RechercheRemote {
	
	public List<Article> rechercheArticles(String nomArticle, BigDecimal prixMinimum, BigDecimal prixMaximum);
	public List rechercheNomArticles();

	public List<Produit> rechercheListeProduits();
	public List<Catalogue> rechercheListeCatalogues();
	public List<MagasinDTO> rechercheArticleParProduit(String nomProduit);
	public List<MagasinDTO> rechercheArticleParCatalogue(long idCatalogue);
	
	public List<Article> rechercheArticleParNom(String nomArticle);
}
